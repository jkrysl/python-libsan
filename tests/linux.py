#!/usr/bin/python

# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function, unicode_literals
import libsan.host.linux as linux


def test_os_info():
    os_info = linux.query_os_info()
    if not os_info:
        print("FAIL: Could not query OS info")
        assert 0

    for info in os_info:
        print("%s: %s" % (info, os_info[info]))
    assert 1


def test_get_boot_device():
    get_boot = linux.get_boot_device()
    if get_boot is None:
        print("FAIL: Could not find '/boot' and '/'! ")
        assert 0
    print("INFO: Boot device is: %s" % get_boot)
    assert 1
